﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace exo4
{
    public partial class BarAdri : Form
    {
        Dictionary<string, int> barAdri = new Dictionary<string, int>();
        public BarAdri()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int.TryParse(textBox2.Text, out int number);
            barAdri.Add(textBox1.Text, number );
            foreach (KeyValuePair<String, int> kvp in barAdri)
            {
                Console.WriteLine($"{kvp.Key} : {kvp.Value}");
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
