﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace exo4
{
    class Program
    {
        static void Main(string[] args)
        {

            /* Créer un Dictionnaire vide {Bar<Nom, qantité>}
             * Boucle
                * Demander à l'utilisateur d'ajouter une clé dans ce dictionnaire
                * Demander à l'utilisateur de spécifier la quantité
             * Afficher l'état du stock
             */

            #region Code via GUI

            //Application.Run(new BarAdri());

            #endregion


            #region Code Console

            string fichier = "Bar.txt";
            Dictionary<string, int> barAdri = new Dictionary<string, int>();
            
            //Verifier si une sauvegarde existe:
            if (File.Exists(fichier))
            {
                // Lecture du fichier existant.
                List<string> historique = File.ReadAllLines(fichier).ToList();

                foreach (string item in historique)
                {
                    barAdri.Add(item.Split(':')[0], int.Parse(item.Split(':')[1]));
                }

                Console.WriteLine("Stock existant dans la dernière sauvegarde");
                foreach (KeyValuePair<String, int> kvp in barAdri)
                {
                    Console.WriteLine($"{kvp.Key} : {kvp.Value}");
                }
                Console.WriteLine(" ");
                Console.WriteLine(" ");
                Console.WriteLine(" ");

            }
            
            Console.WriteLine("Bonjour et bienvenue dans votre bar");
            bool ajout = true;

            while (ajout)
            {
                Console.WriteLine("1.Ajouter");
                Console.WriteLine("2.Modifier");
                Console.WriteLine("3.Supprimer");
                Console.WriteLine("4.Sauvegarder");
                Console.WriteLine("0.Quitter");
                int choix;
                while (!int.TryParse(Console.ReadLine(), out choix))
                {
                    Console.WriteLine("Veuillez rentrer un nombre entier !");
                }

                if (choix == 1)
                {
                    #region Ajouter
                    Console.WriteLine("Veuillez entrer un nouvelle bière!");
                    string entree = Console.ReadLine();
                    entree = entree.ToLower();
                    int nbEntree;
                    while (barAdri.ContainsKey(entree))
                    {
                        Console.WriteLine("Le produit existe déjà ! Entrez un nouveau !");
                        entree = Console.ReadLine();
                    }

                    Console.WriteLine($"Combien désirez vous importer de {entree} dans votre stock ?");
                    while (!int.TryParse(Console.ReadLine(), out nbEntree))
                    {
                        Console.WriteLine("Veuillez rentrer un nombre entier !");
                    }

                    barAdri.Add(entree, nbEntree);


                    #endregion
                }
                 else if (choix == 2)
                {
                    #region Modifier
                    Console.WriteLine(" Vous avez choisi de modifier :");
                    Console.WriteLine("Quel produit ?");
                    String toModif = Console.ReadLine();
                    toModif = toModif.ToLower();
                    while (!barAdri.ContainsKey(toModif))
                    {
                        Console.WriteLine("Le produit que vous cherchez à modifier n'existe pas !");
                        toModif = Console.ReadLine();
                    }
                    Console.WriteLine($"Modifier {toModif} de combien ?");
                    int nbModif = int.Parse(Console.ReadLine());

                    barAdri.TryGetValue(toModif, out int stock);
                    stock = stock + nbModif;

                    barAdri[toModif] = stock;
                    #endregion

                }
                 else if (choix == 3)
                {
                    #region Supprimer
                    Console.WriteLine(" Vous avez choisi de supprimer :");
                    Console.WriteLine("Quel produit ?");
                    String toDelete = Console.ReadLine();
                    toDelete = toDelete.ToLower();

                    while (!barAdri.ContainsKey(toDelete))
                    {
                        Console.WriteLine("Le produit que vous cherchez à supprimer n'existe pas !");
                        toDelete = Console.ReadLine();
                    }
                    barAdri.Remove(toDelete);
                    #endregion
                }
                else if (choix==4)
                {

                    #region Sauvegarder

                    if (!File.Exists(fichier))
                    {
                        FileStream f = File.Create(fichier);
                        f.Close();
                    }

                    List<string> barAdriList = new List<string>();
                    foreach (KeyValuePair<String, int> kvp in barAdri)
                    {
                        barAdriList.Add($"{kvp.Key}:{kvp.Value}");
                    }

                    File.WriteAllLines(fichier, barAdriList); 
                    #endregion

                }
                else if(choix ==0)
                {
                    ajout = false;
                }




                //Affichage du stock
                Console.Clear();
                Console.WriteLine("Etat de votre stock");
                foreach (KeyValuePair<String, int> kvp in barAdri)
                {
                    Console.WriteLine($"{kvp.Key} : {kvp.Value}");
                }



            }

            Console.WriteLine("Vous en avez marre, Press any Key to exit");
            Console.ReadKey();
            #endregion

        }
    }
}
