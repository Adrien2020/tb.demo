﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16_04_2020_Exercice2
{
    class Program
    {
        
        static void Main(string[] args)
        {
            List<int> liste = new List<int>();
            double sommeTotale=0;

            Console.WriteLine("Veuillez entrez des nombres entiers");

            int nombre;
            while (int.TryParse(Console.ReadLine(), out nombre))
            {
                liste.Add(nombre);
            }
            Console.WriteLine(" Vous n'avez pas rentrer un nombre entier !");
            

            for(int i =0; i< liste.Count; i++)
            {
                sommeTotale = sommeTotale + liste[i];
            }
            Console.WriteLine("La moyenne des nombres entrés est :" + (sommeTotale/liste.Count));

            Console.ReadKey();


        }
    }
}
