﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16_04_2020_Exercice1
{
    class Program
    {
        
        static void Main(string[] args)
        {
            int[] tableau = new int[5];
            double sommeTotale=0;

            Console.WriteLine("Veuillez entrez 5 entiers");

            for(int i = 0; i<5; i++)
            {
                int nombre;
                while (!int.TryParse(Console.ReadLine(), out nombre))
                {
                    Console.WriteLine("Entrée incorrecte");
                }
                tableau[i] = nombre;
                Console.WriteLine(" Il vous reste " + (5-(i+1)) + " nombres à rentrer");
            }
            
            Console.WriteLine("Merci, le tabeau est remplit");
            for(int i=0; i< tableau.Length ; i++ )
            {
                Console.WriteLine(tableau[i]);
            }

            foreach(int item in tableau)
            {
                sommeTotale = sommeTotale + item;
            }
            Console.WriteLine("La somme de ce tableau est : " + sommeTotale);
            Console.WriteLine("La moyenne des nombres entiers entrés est : " + (sommeTotale/5) );

            Console.ReadKey();


        }
    }
}
