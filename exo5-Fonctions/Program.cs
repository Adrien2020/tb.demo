﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo5_Fonctions
{
    class Program
    {
        
        static void Main(string[] args)
        {
            //1.1
            Bonjour("Adrien", 28);
            //1.2
            List<int> list = new List<int>() { 1, 6, 4 };

            double mean = CalculMoyenne(list);
            Console.WriteLine("La moyenne de la liste est " + mean);

            //1.3
            Console.WriteLine("Before");
            AfficherList(list);
            
            Console.WriteLine("After");
            AfficherList(ListeDouble(list));
            AfficherList(list);
            //1.4 Créer une fonction qui prend en paramètre une liste<int>
            //et qui ne retourne rien dans laquelle tous les éléments sont élevés au carré.
            AfficherList(list);
            ListeAuCarré(list);
            AfficherList(list);

            /*
             * 1.5 Créer une fonction qui prend en paramètre un nombre n et qui retourne
             * le nième nombre de la suite de Fibonacci (2 solutions bonus: while ou recurvité)
             * */
            Console.Clear();
            Console.WriteLine(FibonnaList(2)); 
            Console.WriteLine(FiboRecur(11));

            /*
             * Créer une fonction qui prend en paramètre un double et qui retourne sa racine carrée
             */

            //SquareRoots(2);
            Console.WriteLine(SquareRootsRecur(81,1));
            Console.WriteLine(SquareRoots(81));
            Console.ReadKey();
        }

        private static double SquareRoots(double v)//, double root)
        {
            double root = 1;

            for (int i = 0; i < 100000000; i++)
            {
                root = (v / root + root) / 2;
            }
            return root;
        }

        private static double SquareRootsRecur(double v, double root)
        {
            if (Math.Abs(v - root * root) < 0.000000001) return root;
            return SquareRootsRecur(v, 0.5 * (root + v / root));
        }

        private static int FibonnaList(int position)
        {
            List<int> Fibo = new List<int>();
            Fibo.Add(0);
            Fibo.Add(1);
            
            for (int i=2;i < (position+2); i++)
            {
                Fibo.Add(Fibo[i - 1] + Fibo[i - 2]);
            }
            
            return Fibo[position];
        }

        private static int FiboRecur(int position)
        {
            if (position == 0)
                return 0;
            else if (position == 1)
                return 1;
            else
            {
                return (FiboRecur(position - 1) + FiboRecur(position - 2));
            } 
        }


        private static void ListeAuCarré(List<int> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i] = list[i] * list[i];
            }
        }

        private static void AfficherList(List<int> list)
        {
            for(int i=0; i<list.Count;i++)
            {
                Console.WriteLine(list[i]);
            }
        }

        private static List<int> ListeDouble(List<int> list)
        {
            List<int> liste = new List<int>();

            for(int i = 0; i<list.Count; i++)
            {
                liste.Add(list[i] * 2);
            }

            return liste;
        }

        public static double CalculMoyenne(List<int> list)
        {
            double sum = 0.0;
            foreach (int item in list)
            {
                sum = sum + item;
            }
            return sum / list.Count;
        }

        private static void Bonjour(string nom, int age)
        {
            Console.WriteLine($"Bonjour je m'appelle {nom} et j'ai {age} ans.");
        }
    }
}
